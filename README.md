# Reuse a SmartPhone as car GPS tracker

## Background

We have a lot of SmartPhone devices no more maintained, no more useful as tipical SmartPhone.
Often we have drawers full of old SmartPhones: sell them is almost difficult and it doesn't worth the time you spend.
What do you think if you can reuse old SmartPhones to do other useful things?

Here we will try to reuse a SmartPhone as a car GPS tracker.

## Software (server) to store tracking coordinates

* [Traccar](https://www.traccar.org/)
* [PhoneTrack](https://apps.nextcloud.com/apps/phonetrack)

## Software (client) to send coordinates

* [Traccar Client](https://www.traccar.org/client/])
* [PhoneTrack Client](https://f-droid.org/packages/net.eneiluj.nextcloud.phonetrack/)
* [GPSLogger](https://gpslogger.app/)
* [μlogger](https://f-droid.org/en/packages/net.fabiszewski.ulogger/)

## Appunti (in italiano)

* Abbiamo bisogno di un pacco batteria esterno, tampone, tra il telefono e la batteria dell'auto?
  * Potrebbe essere indispensabile nei casi di periferiche vecchie dove la batteria è esaurite e/o non sostituibile
  * Andrebbe alimentato/ricaricato solo a quadro acceso
* Come impostare la corretta frequenza di campionamento? Basandosi su quali paramteri?
  * Accelerometri, motore acceso sì/no
* Volume dai scambiati: quale SIM conviene per questo tipo di applicazione?
  * SIM deve essere multioperatore? Deve funzionare all'estero?
* Il dispositivo dovrebbe comunque raccogliere le coordinate anche se non è in grado di inviarle; le invierà tutte assieme ASAP
* Deve poter accettare query via SMS?
  * Invia la tua posizione adesso via SMS
  * Invia la tua posizione adesso al server
* Deve implementare allarmi? Su che canale? (SMS, sul server)
  * Es. accelerometro segnala movimento a motore spento, batteria scarica, ...
* Deve avere IP Pubblico con interfaccia per gestire configurazione?
* Deve potersi collegare ad una VPN?
  * Se sì, sempre con un occhio ai consumi energetici e di banda (WireGuard è più efficinte e resiliente)
* Sofware client (il logger) salva tutti i punti (pericolo "out of space") o si svuota periodicamente?
  * Quello che interessa noi è loggare i punti sul server; la periferica dovrà salvare in locale i punti fin tanto che non li invia al server (funzione di buffer temporaneo)
